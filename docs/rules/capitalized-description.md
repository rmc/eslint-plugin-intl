# Requires descriptions to begin with capitalized word (capitalized-description)

Descriptions are useful for providing information for translators. It is sometimes desirable for 
descriptions to follow a particular style. One element of description formatting styles is whether 
the first word of a description should be capitalized or lowercase.

## Rule Details

This rule aims to enforce a consistent style of descriptions, specifically by either requiring or 
disallowing a capitalized letter as the first character in a description.

By default, this rule will require a non-lowercase letter at the beginning of descriptions.

The following patterns are considered warnings:

```js
defineMessages({ id: 'id', description: 'some invalid description', defaultMessage: 'message'})
```
```jsx
<FormattedMessage id="id" description="some invalid description" defaultMessage="message" />
```

The following patterns are not warnings:

```js
defineMessages({ id: 'id', description: 'Some valid description', defaultMessage: 'message'})
```
```jsx
<FormattedMessage id="id" description="Some valid description" defaultMessage="message" />
```

### Options

This rule has two options: a string value "always" or "never" which determines whether 
capitalization of the first letter of a description should be required or forbidden.

## When Not To Use It

This rule can be disabled if you do not care about the grammatical style of descriptions.
