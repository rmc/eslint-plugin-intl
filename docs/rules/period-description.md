# Enforce or disallow ending the description with a period (period-description)

This rule aims to enforce a consistent style of descriptions, specifically by either requiring or 
disallowing a period as the last character in a description.

By default, this rule will require a period at the end of descriptions.

## Rule Details

This rule aims to enforce a consistent style of descriptions, specifically by either requiring or 
disallowing a period as the last character in a description.

By default, this rule will require a period at the end of descriptions.


The following patterns are considered warnings:

```js
defineMessages({ id: 'id', description: 'Some invalid description', defaultMessage: 'message'})
```
```jsx
<FormattedMessage id="id" description="Some invalid description" defaultMessage="message" />
```

The following patterns are not warnings:

```js
defineMessages({ id: 'id', description: 'Some valid description.', defaultMessage: 'message'})
```
```jsx
<FormattedMessage id="id" description="Some valid description." defaultMessage="message" />
```

### Options

This rule has two options: a string value "always" or "never" which determines whether 
ending the description with a period should be required or forbidden.


## When Not To Use It

This rule can be disabled if you do not care about the grammatical style of descriptions.
