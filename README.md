# eslint-plugin-intl

React Intl specific rules for ESLint

## Installation

You'll first need to install [ESLint](http://eslint.org):

```
$ npm i eslint --save-dev
```

Next, install `eslint-plugin-intl`:

```
$ npm install eslint-plugin-intl --save-dev
```

**Note:** If you installed ESLint globally (using the `-g` flag) then you must also install `eslint-plugin-intl` globally.

## Usage

Add `intl` to the plugins section of your `.eslintrc` configuration file. You can omit the `eslint-plugin-` prefix:

```json
{
    "plugins": [
        "intl"
    ]
}
```


Then configure the rules you want to use under the rules section.

```json
{
    "rules": {
        "intl/rule-name": 2
    }
}
```

## Supported Rules

* [capitalized-description](docs/rules/capitalized-description.md) Requires descriptions to begin with capitalized word
* [period-description](docs/rules/period-description.md) Enforce or disallow ending the description with a period





