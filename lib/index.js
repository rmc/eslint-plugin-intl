/**
 * @fileoverview React Intl specific rules for ESLint
 * @author Ruben Costa
 */
"use strict";

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

var requireIndex = require("requireindex");

//------------------------------------------------------------------------------
// Plugin Definition
//------------------------------------------------------------------------------

// import all rules in lib/rules
module.exports.rules = requireIndex(__dirname + "/rules");

module.exports.configs = {
  recommended: {
    parserOptions: {
      ecmaFeatures: {
        jsx: true
      }
    },
    rules: {
      'react-intl/capitalized-description': [
        2,
        'always'
      ],
      'react-intl/period-description': [
        2,
        'always'
      ],
    }
  }
};



