/**
 * @fileoverview enforce or disallow ending the description with a period
 * @author Ruben Costa
 */
"use strict";

var isDefineMessagesCall = require('../util/isDefineMessagesCall')

var ALWAYS_MESSAGE = "Descriptions should end with a period";
var NEVER_MESSAGE = "Descriptions should not end with a period";
//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------

module.exports = {
  meta: {
    docs: {
      description: "enforce or disallow ending the description with a period",
      category: "Stylistic Issues",
      recommended: false
    },
    fixable: 'code',
    schema: [
      { enum: ["always", "never"] },
    ]
  },

  create: function (context) {

    var enforce = context.options[0] || "always";

    //----------------------------------------------------------------------
    // Helpers
    //----------------------------------------------------------------------

    function isDescriptionValid(description) {
      var lastChar = description.charAt(description.length - 1);
      var isPeriod = lastChar === '.';

      if (enforce === 'always' && !isPeriod) {
        return false;
      } else if (enforce === 'never' && isPeriod) {
        return false;
      }
      return true;
    }

    function addPeriod(string) {
      return string.slice(0, -1) + '.' + string.slice(-1);
    }

    function removePeriod(string) {
      return string.slice(0, -2) + string.slice(-1);
    }

    function processDescription(node) {
      if (!isDescriptionValid(node.value)) {
        context.report(
          {
            node: node,
            message: enforce === "always" ? ALWAYS_MESSAGE : NEVER_MESSAGE,
            fix: function (fixer) {
              return "always" ? fixer.replaceText(node, addPeriod(node.raw)) : fixer.replaceText(
                node,
                removePeriod(node.raw)
              );
            }
          }
        );
      }
    }

    //----------------------------------------------------------------------
    // Public
    //----------------------------------------------------------------------

    return {
      Literal: function (node) {
        if (node.parent.type === 'Property'
          && node.parent.key.name === 'description'
          && isDefineMessagesCall(node)) {
          return processDescription(node)
        }
        if (node.parent.type === 'JSXAttribute'
          && node.parent.name.type === 'JSXIdentifier'
          && node.parent.name.name === 'description'
          && node.parent.parent.type === 'JSXOpeningElement'
          && node.parent.parent.name.type === 'JSXIdentifier'
          && node.parent.parent.name.name === 'FormattedMessage'
        ) {
          return processDescription(node)
        }
      }
    };
  }
};
