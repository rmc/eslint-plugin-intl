/**
 * @fileoverview enforce or disallow capitalization of the first letter of a description
 * @author Ruben Costa
 */
"use strict";

var isDefineMessagesCall = require('../util/isDefineMessagesCall')

var ALWAYS_MESSAGE = "Descriptions should not begin with a lowercase character";
var NEVER_MESSAGE = "Descriptions should not begin with an uppercase character";

//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------

module.exports = {
  meta: {
    docs: {
      description: "enforce or disallow capitalization of the first letter of a description",
      category: "Stylistic Issues",
      recommended: false
    },
    fixable: 'code',
    schema: [
      { enum: ["always", "never"] },
    ]
  },

  create: function (context) {

    var capitalize = context.options[0] || "always";

    //----------------------------------------------------------------------
    // Helpers
    //----------------------------------------------------------------------

    function isDescriptionValid(description) {
      var firstChar = description.charAt(0);
      var isUppercase = firstChar !== firstChar.toLocaleLowerCase();
      var isLowercase = firstChar !== firstChar.toLocaleUpperCase();

      if (capitalize === 'always' && isLowercase) {
        return false;
      } else if (capitalize === 'never' && isUppercase) {
        return false;
      }
      return true;
    }

    function capitalizeFix(string) {
      return string.slice(0, 1) + string.charAt(1).toLocaleUpperCase() + string.slice(2);
    }

    function uncapitalizeFix(string) {
      return string.slice(0, 1) + string.charAt(1).toLocaleLowerCase() + string.slice();
    }

    function processDescription(node) {
      if (!isDescriptionValid(node.value)) {
        context.report(
          {
            node: node,
            message: capitalize === "always" ? ALWAYS_MESSAGE : NEVER_MESSAGE,
            fix: function (fixer) {
              return "always"
                ? fixer.replaceText(node, capitalizeFix(node.raw))
                : fixer.replaceText(
                node,
                uncapitalizeFix(node.raw)
              );
            }
          }
        )
      }
    }

    //----------------------------------------------------------------------
    // Public
    //----------------------------------------------------------------------

    return {
      Literal: function (node) {
        if (node.parent.type === 'Property'
          && node.parent.key.name === 'description'
          && isDefineMessagesCall(node)) {
          return processDescription(node)
        }
        if (node.parent.type === 'JSXAttribute'
          && node.parent.name.type === 'JSXIdentifier'
          && node.parent.name.name === 'description'
          && node.parent.parent.type === 'JSXOpeningElement'
          && node.parent.parent.name.type === 'JSXIdentifier'
          && node.parent.parent.name.name === 'FormattedMessage'
        ) {
          return processDescription(node)
        }
      }
    };
  }
};
