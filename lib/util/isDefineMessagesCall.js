function isDefineMessagesCall(node) {
  if (node.parent.type === 'Property'
    && node.parent.parent.type === 'ObjectExpression'
    && node.parent.parent.parent
    && node.parent.parent.parent.type === 'Property') {
    return isDefineMessagesCall(node.parent.parent)
  } else if (node.parent.type === 'Property'
    && node.parent.parent.type === 'ObjectExpression'
    && node.parent.parent.parent
    && node.parent.parent.parent.type === 'CallExpression'
    && node.parent.parent.parent.callee.name === 'defineMessages') {
    return true;
  } else {
    return false;
  }
}

module.exports = isDefineMessagesCall;
