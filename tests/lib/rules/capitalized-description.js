/**
 * @fileoverview Requires descriptions to begin with capitalized word
 * @author Ruben Costa
 */
"use strict";

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

var rule = require("../../../lib/rules/capitalized-description");

var RuleTester = require("eslint").RuleTester;

var parserOptions = {
  ecmaVersion: 6,
  ecmaFeatures: {
    experimentalObjectRestSpread: true,
    jsx: true
  }
};

//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------

var ruleTester = new RuleTester();
ruleTester.run(
  "capitalized-description", rule, {

    valid: [
      "defineMessages({ id: 'id', description: 'Some description', defaultMessage: 'message'})",
      {
        code: "defineMessages({ id: 'id', description: 'Some description', defaultMessage: 'message'})",
        options: ["always"]
      },
      {
        code: "defineMessages({ id: 'id', description: 'some description', defaultMessage: 'message'})",
        options: ["never"]
      },
      {
        code: '<FormattedMessage {...messageDescriptor} />',
        parserOptions: parserOptions,
      },
      {
        code: '<FormattedMessage id="id" description="Some description" defaultMessage="message" />',
        parserOptions: parserOptions,
      },
    ],

    invalid: [
      {
        code: '<FormattedMessage id="id" description="some description" defaultMessage="message" />',
        errors: [
          {
            message: "Descriptions should not begin with a lowercase character",
            type: "Literal"
          }
        ],
        parserOptions: parserOptions,
      },
      {
        code: "defineMessages({ id: 'id', description: 'some description', defaultMessage: 'message'})",
        errors: [
          {
            message: "Descriptions should not begin with a lowercase character",
            type: "Literal"
          }
        ]
      },
      {
        code: `
          const messages = {
            foo: defineMessages(
              {
                bar: {
                  id: 'id',
                  description: 'some description',
                  defaultMessage: 'message',
                },
                baz: {
                  id: 'other.id',
                  description: 'some other description',
                  defaultMessage: 'other message',
                },
              }
            ),
          }
          `,
        errors: [
          {
            message: "Descriptions should not begin with a lowercase character",
            type: "Literal"
          },
          {
            message: "Descriptions should not begin with a lowercase character",
            type: "Literal"
          }
        ],
        parserOptions: parserOptions,
      },
      {
        code: "defineMessages({ id: 'id', description: 'some description', defaultMessage: 'message'})",
        options: ["always"],
        errors: [
          {
            message: "Descriptions should not begin with a lowercase character",
            type: "Literal"
          }
        ]
      },
      {
        code: "defineMessages({ id: 'id', description: 'Some description', defaultMessage: 'message'})",
        options: ["never"],
        errors: [
          {
            message: "Descriptions should not begin with an uppercase character",
            type: "Literal"
          }
        ]
      }
    ]
  }
);
