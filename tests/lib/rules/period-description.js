/**
 * @fileoverview enforce or disallow ending the description with a period
 * @author Ruben Costa
 */
"use strict";

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

var rule = require("../../../lib/rules/period-description");

var RuleTester = require("eslint").RuleTester;

var parserOptions = {
  ecmaVersion: 6,
  ecmaFeatures: {
    experimentalObjectRestSpread: true,
    jsx: true
  }
};
//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------

var ruleTester = new RuleTester();
ruleTester.run(
  "period-description", rule, {
    valid: [
      "defineMessages({ id: 'id', description: 'some description.', message: 'message' })",
      {
        code: "defineMessages({ id: 'id', description: 'some description.', defaultMessage: 'message'})",
        options: ["always"]
      },
      {
        code: "defineMessages({ id: 'id', description: 'some description', defaultMessage: 'message'})",
        options: ["never"]
      },
      {
        code: '<FormattedMessage {...messageDescriptor} />',
        parserOptions: parserOptions,
      },
      {
        code: '<FormattedMessage id="id" description="some description." defaultMessage="message" />',
        parserOptions: parserOptions,
      },
      {
        code: '<FormattedMessage id="id" description="some description" defaultMessage="message" />',
        parserOptions: parserOptions,
        options: ["never"],
      },
    ],
    invalid: [
      {
        code: "defineMessages({ id: 'id', description: 'some invalid description', message: 'message' })",
        errors: [{ message: "Descriptions should end with a period", type: "Literal" }]
      },
      {
        code: "defineMessages({ id: 'id', description: 'some invalid description.', message: 'message' })",
        errors: [{ message: "Descriptions should not end with a period", type: "Literal" }],
        options: ["never"]
      },
      {
        code: `
          const messages = {
            foo: defineMessages(
              {
                bar: {
                  id: 'id',
                  description: 'some description',
                  defaultMessage: 'message',
                },
                baz: {
                  id: 'other.id',
                  description: 'some other description',
                  defaultMessage: 'other message',
                },
              }
            ),
          }
          `,
        errors: [
          {
            message: "Descriptions should end with a period",
            type: "Literal"
          },
          {
            message: "Descriptions should end with a period",
            type: "Literal"
          }
        ],
        parserOptions: parserOptions,
      }
    ]
  }
);
